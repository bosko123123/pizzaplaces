//
//  Database.m
//  PizzaPlaces
//
//  Created by Bosko Petreski on 6/18/15.
//  Copyright (c) 2015 Bosko Petreski. All rights reserved.
//

#import "Database.h"

@implementation Database
+(NSManagedObjectContext *)managedObjectContext {
    NSManagedObjectContext *context = nil;
    id delegate = [[UIApplication sharedApplication] delegate];
    if ([delegate performSelector:@selector(managedObjectContext)]) {
        context = [delegate managedObjectContext];
    }
    return context;
}
+(BOOL)save:(NSDictionary *)dict toTable:(NSString *)table{
    NSManagedObjectContext *context = [self managedObjectContext];
    
    // Create a new managed object
    NSManagedObject *newDevice = [NSEntityDescription insertNewObjectForEntityForName:table inManagedObjectContext:context];
    
    for(NSString *key in dict){
        [newDevice setValue:dict[key] forKey:key];
    }
    
    NSError *error = nil;
    // Save the object to persistent store
    if (![context save:&error]) {
        NSLog(@"Can't Save! %@ %@", error, [error localizedDescription]);
        return NO;
    }
    else{
        return YES;
    }
}
+(NSArray *)getAllElementFromTable:(NSString *)table{
    NSManagedObjectContext *managedObjectContext = [self managedObjectContext];
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] initWithEntityName:table];
    return [[managedObjectContext executeFetchRequest:fetchRequest error:nil] mutableCopy];
}
+(NSArray *)getAllElementFromTable:(NSString *)table where:(NSPredicate *)predicate{
    NSManagedObjectContext *managedObjectContext = [self managedObjectContext];
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] initWithEntityName:table];
    [fetchRequest setPredicate:predicate];
    return [[managedObjectContext executeFetchRequest:fetchRequest error:nil] mutableCopy];
}
+(NSArray *)getAllElementFromTable:(NSString *)table where:(NSPredicate *)predicate andOrderBy:(NSArray *)pOrder andLimit:(int)pLimit{
    NSManagedObjectContext *managedObjectContext = [self managedObjectContext];
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] initWithEntityName:table];
    [fetchRequest setPredicate:predicate];
    [fetchRequest setSortDescriptors:pOrder];
    [fetchRequest setFetchLimit:pLimit];
    return [[managedObjectContext executeFetchRequest:fetchRequest error:nil] mutableCopy];
}
+(NSArray *)getAllElementFromTable:(NSString *)table where:(NSPredicate *)predicate andOrderBy:(NSArray *)pOrder{
    NSManagedObjectContext *managedObjectContext = [self managedObjectContext];
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] initWithEntityName:table];
    [fetchRequest setPredicate:predicate];
    [fetchRequest setSortDescriptors:pOrder];
    return [[managedObjectContext executeFetchRequest:fetchRequest error:nil] mutableCopy];
}
+(NSArray *)getAllElementFromTable:(NSString *)table orderBy:(NSArray *)pOrder{
    NSManagedObjectContext *managedObjectContext = [self managedObjectContext];
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] initWithEntityName:table];
    [fetchRequest setSortDescriptors:pOrder];
    return [[managedObjectContext executeFetchRequest:fetchRequest error:nil] mutableCopy];
}
+(BOOL)removeElement:(NSManagedObject *)object{
    NSManagedObjectContext *context = [self managedObjectContext];
    // Delete object from database
    [context deleteObject:object];
    
    NSError *error = nil;
    if (![context save:&error]) {
        NSLog(@"Can't Delete! %@ %@", error, [error localizedDescription]);
        return NO;
    }
    else{
        return YES;
    }
}
+(void)deleteAllObjects:(NSString *)entityDescription{
    NSManagedObjectContext *context = [self managedObjectContext];

    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:entityDescription inManagedObjectContext:context];
    [fetchRequest setEntity:entity];
    
    NSError *error;
    NSArray *items = [context executeFetchRequest:fetchRequest error:&error];

    for (NSManagedObject *managedObject in items) {
        [context deleteObject:managedObject];
    }
    if (![context save:&error]) {
        NSLog(@"Error deleting %@ - error:%@",entityDescription,error);
    }
    
}
+(BOOL)update:(NSManagedObject *)object withDict:(NSDictionary *)dict toTable:(NSString *)table{
    NSManagedObjectContext *context = [self managedObjectContext];
    
    for(NSString *key in dict){
        [object setValue:dict[key] forKey:key];
    }
    
    NSError *error = nil;
    // Save the object to persistent store
    if (![context save:&error]) {
        NSLog(@"Can't Save! %@ %@", error, [error localizedDescription]);
        return NO;
    }
    else{
        return YES;
    }
}
@end
