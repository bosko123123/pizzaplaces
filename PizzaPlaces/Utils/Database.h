//
//  Database.h
//  PizzaPlaces
//
//  Created by Bosko Petreski on 6/18/15.
//  Copyright (c) 2015 Bosko Petreski. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AppDelegate.h"
#import <CoreData/CoreData.h>

@interface Database : NSObject
+(NSManagedObjectContext *)managedObjectContext;
+(BOOL)save:(NSDictionary *)dict toTable:(NSString *)table;
+(BOOL)removeElement:(NSManagedObject *)object;
+(BOOL)update:(NSManagedObject *)object withDict:(NSDictionary *)dict toTable:(NSString *)table;
+(void)deleteAllObjects:(NSString *)entityDescription;
+(NSArray *)getAllElementFromTable:(NSString *)table;
+(NSArray *)getAllElementFromTable:(NSString *)table orderBy:(NSArray *)pOrder;
+(NSArray *)getAllElementFromTable:(NSString *)table where:(NSPredicate *)predicate;
+(NSArray *)getAllElementFromTable:(NSString *)table where:(NSPredicate *)predicate andOrderBy:(NSArray *)pOrder;
+(NSArray *)getAllElementFromTable:(NSString *)table where:(NSPredicate *)predicate andOrderBy:(NSArray *)pOrder andLimit:(int)pLimit;
@end
