//
//  APICalls.m
//  PizzaPlaces
//
//  Created by Bosko Petreski on 6/18/15.
//  Copyright (c) 2015 Bosko Petreski. All rights reserved.
//

#import "APICalls.h"
#import "AppDelegate.h"

@implementation APICalls

+(void)GetNearPizzaPlacesSuccess:(BlockSuccess)success failed:(BlockFail)fail{
    
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];

    double gpsLat = appDelegate.gpsLat;
    double gpsLong = appDelegate.gpsLong;
    
    NSString *strURL = [NSString stringWithFormat:@"https://maps.googleapis.com/maps/api/place/nearbysearch/json?location=%f,%f&radius=1000&types=food&name=pizza&key=AIzaSyAvwx4qWZuztWLM4uSVdpRw8OeZYiZX8oQ",gpsLat,gpsLong];
    
    strURL = [strURL stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    
    [NSURLConnection sendAsynchronousRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:strURL]] queue:[NSOperationQueue mainQueue] completionHandler:^(NSURLResponse *response, NSData *data, NSError *connectionError) {
        
        if(!connectionError){
            NSDictionary *dict = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:&connectionError];
            if(!connectionError){
                success(dict);
            }
            else{
                fail();
            }
        }
        else{
            fail();
        }
    }];
    
}

@end
