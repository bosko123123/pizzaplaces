//
//  APICalls.h
//  PizzaPlaces
//
//  Created by Bosko Petreski on 6/18/15.
//  Copyright (c) 2015 Bosko Petreski. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef void(^BlockSuccess)(NSDictionary *dict);
typedef void(^BlockFail)();

@interface APICalls : NSObject

+(void)GetNearPizzaPlacesSuccess:(BlockSuccess)success failed:(BlockFail)fail;

@end
