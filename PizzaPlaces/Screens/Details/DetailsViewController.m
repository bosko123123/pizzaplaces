//
//  DetailsViewController.m
//  PizzaPlaces
//
//  Created by Bosko Petreski on 6/18/15.
//  Copyright (c) 2015 Bosko Petreski. All rights reserved.
//

#import "DetailsViewController.h"

@interface DetailsViewController ()

@end

@implementation DetailsViewController

#pragma mark - UIViewDelegates
-(void)viewDidLoad {
    [super viewDidLoad];
    
    lblAddress.text = self.placeInfo.address;
    lblDistance.text = self.placeInfo.distance.stringValue;
    lblName.text = self.placeInfo.name;
    lblRating.text = self.placeInfo.rating.stringValue;
}
-(void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

@end
