//
//  DetailsViewController.h
//  PizzaPlaces
//
//  Created by Bosko Petreski on 6/18/15.
//  Copyright (c) 2015 Bosko Petreski. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PizzaPlace.h"

@interface DetailsViewController : UIViewController{
    IBOutlet UILabel *lblName;
    IBOutlet UILabel *lblAddress;
    IBOutlet UILabel *lblDistance;
    IBOutlet UILabel *lblRating;
}

@property (nonatomic,strong) PizzaPlace *placeInfo;

@end
