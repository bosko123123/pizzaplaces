//
//  PlaceCell.h
//  PizzaPlaces
//
//  Created by Bosko Petreski on 6/18/15.
//  Copyright (c) 2015 Bosko Petreski. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PlaceCell : UITableViewCell

@property (nonatomic,strong) IBOutlet UILabel *lblName;
@property (nonatomic,strong) IBOutlet UILabel *lblAddress;

@end
