//
//  ViewController.m
//  PizzaPlaces
//
//  Created by Bosko Petreski on 6/18/15.
//  Copyright (c) 2015 Bosko Petreski. All rights reserved.
//

#import "ViewController.h"
#import "PlaceCell.h"
#import "DetailsViewController.h"
#import "APICalls.h"
#import "Database.h"
#import "AppDelegate.h"
#import "PizzaPlace.h"

@interface ViewController ()

@end

@implementation ViewController

#pragma mark - CustomFunctions
-(double)calculateDistance:(NSDictionary *)pDict{
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    double gpsLat = appDelegate.gpsLat;
    double gpsLong = appDelegate.gpsLong;
    
    double locationLat = [pDict[@"lat"] doubleValue];
    double locationLong = [pDict[@"long"] doubleValue];
    
    CLLocation *locA = [[CLLocation alloc] initWithLatitude:gpsLat longitude:gpsLong];
    CLLocation *locB = [[CLLocation alloc] initWithLatitude:locationLat longitude:locationLong];
    CLLocationDistance distance = [locA distanceFromLocation:locB];
    return distance;
}
-(void)saveToDatabase:(NSArray *)pArr{
    
    for(NSDictionary *dict in pArr){
        [Database save:@{@"address":dict[@"vicinity"],
                         @"distance":@([self calculateDistance:dict[@"geometry"][@"location"]]),
                         @"name":dict[@"name"],
                         @"rating":@([dict[@"rating"] isEqual:[NSNull null]] ? [@"0.0" doubleValue] : [dict[@"rating"] doubleValue])} toTable:@"PizzaPlace"];
    }
}

#pragma mark - UITableViewDelegates
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return arrPlaces.count;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    PlaceCell *cell = (PlaceCell *)[tableView dequeueReusableCellWithIdentifier:@"PlaceCell" forIndexPath:indexPath];
    
    PizzaPlace *place = arrPlaces[indexPath.row];
    
    cell.lblName.text = place.name;
    cell.lblAddress.text = place.address;
    
    return cell;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    PizzaPlace *place = arrPlaces[indexPath.row];

    DetailsViewController *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"DetailsViewController"];
    controller.placeInfo = place;
    [self.navigationController pushViewController:controller animated:YES];
}

#pragma mark - UIViewDelegates
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [[NSNotificationCenter defaultCenter] addObserverForName:@"NewUpdate" object:nil queue:[NSOperationQueue mainQueue] usingBlock:^(NSNotification *note) {
        
        [APICalls GetNearPizzaPlacesSuccess:^(NSDictionary *dict) {
            if([dict[@"results"] count] > 0){
                [Database deleteAllObjects:@"PizzaPlace"];
                [self saveToDatabase:dict[@"results"]];
                
                NSPredicate *predicate = [NSPredicate predicateWithFormat:@"distance > %d",1000];
                NSSortDescriptor *orderName = [[NSSortDescriptor alloc] initWithKey:@"name" ascending:YES];
                arrPlaces = [Database getAllElementFromTable:@"PizzaPlace" where:predicate andOrderBy:@[orderName] andLimit:5];
                [tblPlaces reloadData];
            }
            else{
                NSLog(@"NO LOCATIONS");
            }
        } failed:^{
            NSLog(@"failed");
        }];
    }];
}
-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"NewUpdate" object:nil];
}
-(void)viewDidLoad{
    [super viewDidLoad];
}
-(void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

@end
