//
//  ViewController.h
//  PizzaPlaces
//
//  Created by Bosko Petreski on 6/18/15.
//  Copyright (c) 2015 Bosko Petreski. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController <UITableViewDataSource,UITableViewDelegate>{
    IBOutlet UITableView *tblPlaces;
    NSArray *arrPlaces;
}

@end

