//
//  PizzaPlace.m
//  PizzaPlaces
//
//  Created by Bosko Petreski on 6/18/15.
//  Copyright (c) 2015 Bosko Petreski. All rights reserved.
//

#import "PizzaPlace.h"


@implementation PizzaPlace

@dynamic name;
@dynamic address;
@dynamic rating;
@dynamic distance;

@end
